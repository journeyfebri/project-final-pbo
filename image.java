package plant;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class image {
    public static void view(String img, String title) throws IOException, InterruptedException {
       File file = new File("plant\\"+img);
       BufferedImage imgRead = ImageIO.read(file);
       ImageIcon imageIcon = new ImageIcon(imgRead);
       JFrame jFrame = new JFrame(title);
       
       jFrame.setLayout(new FlowLayout());
       jFrame.setSize(imgRead.getWidth()+100, imgRead.getHeight()+100);
       JLabel jlabel = new JLabel();

       jlabel.setIcon(imageIcon);
       jFrame.add(jlabel);
       jFrame.setVisible(true);

       jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       TimeUnit.SECONDS.sleep(3);
       jFrame.dispose();
    }
}
